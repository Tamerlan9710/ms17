package com.example.ms17.configuration;

import com.example.ms17.dp.Singleton;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    public Singleton newSingleton(){
        return  new Singleton();
    }

}
