package com.example.ms17;

import com.example.ms17.dp.Singleton;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class Ms17Application implements CommandLineRunner {
    private final Singleton s;



    public static void main(String[] args) {
        SpringApplication.run(Ms17Application.class, args);


    }

    @Override
    public void run(String... args) throws Exception {
        s.hello();
    }
}
